% Domestic Violence, Technology, and You
% Andrew Lewman, andrew@torproject.org
% 12 February 2014

# Who is this guy?

501(c)(3) non-profit organization dedicated to the research and development of technologies for online anonymity and privacy. 

[https://www.torproject.org](https://www.torproject.org)

\begin{center}
\includegraphics[keepaspectratio,width=\textwidth, height=.8\textheight]{images/2009-oval_sticker_new}
\end{center}

# Victims of Abuse

Working with victims of infectious surveillance and trafficking.

Provide anonymity and control of identity for the victims.

Consult with law enforcement, advocacy organizations, and others to understand technology capabilities and usage in trafficking situations.

# Infectious Surveillance

  - What is it?

# Infectious Surveillance

  - 100 bucks is all it takes to ruin a life and those trying to help

# Infectious Surveillance

  - Draft clean communications protocol, [https://wiki.tpo.is/](https://wiki.tpo.is/)

# Working with Advocates

  - Working with NNEDV, [https://blog.torproject.org/blog/ending-domestic-violence-nnedv-and-tor](https://blog.torproject.org/blog/ending-domestic-violence-nnedv-and-tor)
  - Transition House, [http://www.transitionhouse.org/](http://www.transitionhouse.org/)
  - Emerge, [http://emergedv.com/](http://emergedv.com/)
  - REACH Beyond Domestic Violence, [http://reachma.org](http://reachma.org)

# Working with Law Enforcement

  - FBI, [https://blog.torproject.org/blog/trip-report-october-fbi-conference](https://blog.torproject.org/blog/trip-report-october-fbi-conference)
  - UK SOCA, [https://blog.torproject.org/blog/meeting-soca-london](https://blog.torproject.org/blog/meeting-soca-london)
  - DHS, ICE, DoJ, and others.

# Success

See 2 case studies at [https://wiki.tpo.is/Case%20Studies](https://wiki.tpo.is/Case%20Studies)

 - Elderly worker
 - Adult performer

Fuerza [http://fuerza.is](http://fuerza.is) mobile app to help diagnose stalking/surveillance

# Challenges

Lack of data on technology and DV

Lack of awareness that "cyber" stalking/harassment is trauma inducing like physical abuse

Relevant tech help for victims


# Thanks!

\begin{center}
\includegraphics[keepaspectratio,width=\textwidth, height=.8\textheight]{images/thankyou_img}
\end{center}