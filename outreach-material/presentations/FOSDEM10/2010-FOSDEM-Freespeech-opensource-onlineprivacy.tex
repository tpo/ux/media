\documentclass{beamer}
\mode<presentation>
\usetheme{Pittsburgh}
\usecolortheme{beaver}
\title{Understanding, Growing, \& Extending Online Anonymity with Tor}
\author{Andrew Lewman \\ andrew@torproject.org}
\date{07 Feb 2010}
\begin{document}

\begin{frame}
\maketitle  
\begin{center}
\includegraphics[height=3cm]{./images/2009-tor-logo}
\hspace{1cm} 
\includegraphics[height=3cm]{./images/mullah}
\end{center}
\end{frame}
  
\begin{frame}
\frametitle{What is anonymity?}
\includegraphics[width=10cm]{./images/2llg3ts}
\end{frame}

\begin{frame}
\frametitle{Anonymity isn't cryptography}
\begin{itemize}
\item Cryptography protects the contents in transit
\item You still know who is talking to whom, how often, and how much data is sent.
\end{itemize}
\begin{center}
\includegraphics[width=5cm]{./images/encryption-cc-by-sa}
\end{center}
\end{frame}

\begin{frame}
\frametitle{Anonymity isn't steganography}
Attacker can tell Alice is talking to someone, how often, and how much data is sent.
\bigskip

\begin{center}
\includegraphics[width=5cm]{./images/steganography-cc-by-sa}
\end{center}
\end{frame}

\begin{frame}
\frametitle{Anonymity isn't just wishful thinking...}
\begin{itemize}
\item "You can't prove it was me!"
\pause \item "Promise you won't look"
\pause \item "Promise you won't remember"
\pause \item "Promise you won't tell"
\pause \item "I didn't write my name on it!"
\pause \item "Isn't the Internet already anonymous?"
\end{itemize}
\end{frame}

\begin{frame}
\frametitle{..since "weak" isn't anonymity.}
\begin{itemize}
\item \textit{"You can't prove it was me!"}  Proof is a very \textbf{strong} word.  Statistical analysis allows suspicion to become certainty.
\pause \item \textit{"Promise you won't look/remember/tell"}  Will other parties have the abilities and incentives to keep these promises?
\pause \item \textit{"I didn't write my name on it!"}  Not what we're talking about.
\pause \item \textit{"Isn't the Internet already anonymous?"}  Nope!
\end{itemize}
\end{frame}

\begin{frame}
\frametitle{Who wants anonymity online?}
\begin{itemize}
\item Ordinary people
	\begin{itemize}
	\item To avoid personal information being sold to marketers
	\item Protect themselves when researching sensitive topics
	\end{itemize}
\item Militaries and law enforcement
	\begin{itemize}
	\item To carry out intelligence gathering
	\item Protect undercover field agents
	\item Offer anonymous tip lines
	\end{itemize}
\item Journalists
	\begin{itemize}
	\item To protect sources, such as whistle blowers
	\end{itemize}
\item Human rights workers
	\begin{itemize}
	\item To publicize abuses and protect themselves from surveillance
	\item Blogging about controversial subjects
	\end{itemize}
\item Businesses
	\begin{itemize}
	\item To observe their competition and build anonymous collaborations
	\end{itemize}
\end{itemize}
\end{frame}

\begin{frame}
\frametitle{Anonymous communication}
\begin{itemize}
\item People have to hide in a crowd of other people ("anonymity loves company")
\item The goal of the system is to make all users look as similar as possible, to give a bigger crowd
\item Hide who is communicating with whom
\item Layered encryption and random delays hide correlation between input traffic and output traffic
\end{itemize}
\end{frame}

\begin{frame}
\frametitle{Low versus High-latency anonymous communication systems}
\begin{itemize}
\item Tor is not the first system; ZKS, mixmaster, single-hop proxies, Crowds, Java Anon Proxy.
\item Low-latency systems are vulnerable to end-to-end correlation attacks.
\item High-latency systems are more resistant to end-to-end correlation attacks, but by definition, less interactive.
\end{itemize}
\end{frame}

\begin{frame}
\frametitle{Low-latency systems are generally more attractive to today's user}
\begin{itemize}
\item Interactive apps: web, instant messaging, VOIP, ssh, X11, cifs/nfs, video streaming (millions of users)
\item Multi-hour delays: email, nntp, blog posting? (tens of thousands of users?)
\pause \item \begin{center}\begin{Large}And if anonymity loves company...\end{Large}\end{center}
\end{itemize}
\end{frame}

\begin{frame}
\frametitle{What is Tor?}
\begin{itemize}
\item online anonymity, circumvention software and network
\item open source, free software (BSD 3-clause \& GPLv2 licenses)
\pause \item active research environment: \\
Rice, UMN, NSF, NRL, Drexel, Waterloo, Cambridge UK, Bamberg Germany, Boston U, Harvard, MIT, RPI, GaTech
\pause \item increasingly diverse toolset: \\
Tor, Torbutton, Tor Browser Bundle, TorVM, Incognito LiveCD, Tor Weather, Tor auto-responder, Secure Updater, Orbot, TorFox, Torora, Portable Tor, Tor Check, Arm, Nymble, Tor Control, Tor Wall
\end{itemize}
\end{frame}

\begin{frame}
\frametitle{Who is The Tor Project, Inc?}
\begin{columns}[c]
\column{5cm}
\includegraphics[height=4.5cm]{./images/2009-oval_sticker_new}
\column{5cm}
The 501(c)(3) non-profit organization dedicated to the research and development of tools for online anonymity and privacy
\end{columns}
\end{frame}

\begin{frame}
\frametitle{Tor is a low-latency anonymity system}
\begin{itemize}
\item Based on technology developed in the Onion Routing project
\item Privacy by design, not by policy (no data collected)
\item Commonly used for web browsing and instant messaging (works for any TCP traffic)
\item Originally built as a pure anonymity system (hides who is talking to whom)
\item Now designed to resist censorship too (hides whether someone is using the system at all)
\item Centralized directory authorities publish a list of all servers
\end{itemize}

\begin{center}
\includegraphics[height=3cm]{./images/2009-tor-logo}
\end{center}
\end{frame}

\begin{frame}
\frametitle{Tor code stats}
\includegraphics[width=10.5cm]{./images/tor-lines-of-code-by-language}
\flushright \tiny stats from ohloh.net
\end{frame}

\begin{frame}
\frametitle{Tor hides communication patterns by relaying data through volunteer servers}
\begin{center}
\begin{overlayarea}{9cm}{6cm}
      \only<1>{\includegraphics[width=9cm]{./images/tor-network}}
      \only<2>{\includegraphics[width=9cm]{./images/tor-safe-selection}}
      \only<3>{\includegraphics[width=9cm]{./images/tor-safe-path}}
      \end{overlayarea}
      \flushright

      \tiny Diagram: Robert Watson
    \end{center}
  \end{frame}

\begin{frame}
\frametitle{How many people use Tor?}
It's an anonymity system. \\[1cm]
\pause http://metrics.torproject.org/ for an idea.
\end{frame}

\begin{frame}
\frametitle{How many people use Tor?}
\begin{flushleft}
\includegraphics[width=12cm]{./images/2009-12-16-mozilla-stats}
\end{flushleft}
\end{frame}

\begin{frame}
\frametitle{Tor hidden services allow privacy enhanced hosting of services}
\includegraphics[scale=0.5]{./images/wikileaks}
\end{frame}
  
\begin{frame}
\frametitle{How is Tor different from other systems?}
\begin{overlayarea}{9cm}{6cm}
\only<1>{\includegraphics[height=7cm]{./images/single_hop_relay}}
\only<2>{\includegraphics[height=7cm]{./images/evil_single_hop_relay}}
\only<3>{\includegraphics[height=7cm]{./images/data_snooping_single_hop_relay}}
\end{overlayarea}
\end{frame}

\begin{frame}
\frametitle{Universal Declaration of Human Rights}
\textbf{Article 19}
\medskip
\begin{quotation}
\noindent \includegraphics[width=1cm]{./images/opquo}\quad Everyone has the right to freedom of opinion and expression; this right includes freedom to hold opinions without interference and to seek, receive and impart information and ideas through any media and regardless of frontiers.
\end{quotation}

\bigskip
\textbf{Article 20}
\medskip
\begin{quotation}
\noindent \includegraphics[width=1cm]{./images/opquo}\quad Everyone has the right to freedom of peaceful assembly and association.
\end{quotation}
\end{frame}

\begin{frame}
\frametitle{George Orwell was an optimist}
\begin{quotation}
\noindent \includegraphics[width=1cm]{./images/opquo}\quad Who controls the past, controls the future: who controls the present controls the past
\end{quotation}
\flushright --- George Orwell, Nineteen Eighty Four, 1949

\flushleft
The re-writing of history is now much more efficient than when George Orwell imagined armies of Winston Smiths cutting holes in newspaper archives.
\end{frame}

\begin{frame}
\frametitle{Resisting Internet censorship}

\begin{quotation}
\noindent \includegraphics[width=1cm]{./images/opquo}\quad The Net interprets censorship as damage and routes around it.
\end{quotation}
\flushright --- John Gilmore, 1993

\flushleft
No longer true on a technical level: censorship is in the routers.
\medskip

Remains true on a social level: when material is censored, people distribute copies and draw attention to them
\medskip

But what if people are too afraid to do this?
\end{frame}

\begin{frame}
  \frametitle{Internet surveillance is pervasive}
     \begin{columns}[t]
      \column{7cm}
\begin{itemize}
\item Conventional surveillance methods had to be targeted
\item Internet censorship is capable of monitoring everyone, all of the time
\item Governments are increasing monitoring: SORM (Russia), Golden Shield (China), Data Retention Directive (EU), and Interception Modernisation Programme (UK)
\item 1 in 7 East German citizens worked for the Stasi. Today we can achieve the same results with a fraction of the cost
\end{itemize}

      \column{5cm}
     \vbox{}
    \parbox{5cm}{\includegraphics[width=4.5cm]{./images/nsa-room}}
    \end{columns}
\end{frame}

\begin{frame}
\frametitle{Traffic data surveillance}
\begin{itemize}
\item Traffic data (who talks to whom, how often and for how long) is the core of intelligence capabilities
\item This information is cheaper to record and store, compared to full content
\item Because it can be easily processed by computer, data mining techniques can be used to understand social structures
\end{itemize}

\begin{quotation}
\noindent \includegraphics[width=1cm]{./images/opquo}\quad No government of any colour is to be trusted with such a roadmap to our souls
\end{quotation}
\flushright --- Sir Ken Macdonald, former director of public prosecutions, on the UK Interception Modernisation Program
\end{frame}

\begin{frame}
\frametitle{Importantly, information on social networks can be derived}
\begin{columns}[t]
\column{4.5cm}
\vspace{0cm}
\noindent {\color{red}$\bullet$} Communities\newline
{\color{blue}$\bullet$} People
\column{7cm}
\vbox{}
\includegraphics[clip, trim=5cm 12cm 2cm 5cm,width=7cm]{./images/network}
\end{columns}
\flushright{\tiny From "The Economics of Mass Surveillance" by George Danezis and Bettina Wittneben}
\end{frame}

\begin{frame}
\frametitle{Supporters}
\includegraphics[scale=.5]{./images/167234088_08f07f0dbe_o}
\end{frame}

\begin{frame}
\frametitle{How to get involved}
\begin{center}
\begin{huge}https://torproject.org/volunteer\end{huge}
\end{center}
\end{frame}
  
\begin{frame}
\frametitle{Credits}
\begin{itemize}
\item Thank you to Steven J. Murdoch, \url{http://www.cl.cam.ac.uk/users/sjm217/}, for the research and basis for the latter parts of the presentation. \\
\item Photographer and Diagram credits as listed throughout the presentation.
\end{itemize}
\end{frame}

\end{document}
