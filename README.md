## Media

This project contains an archive of older Tor assets and materials. It is no longer maintained.

Recent outreach materials can be found on [community.torproject.org](https://community.torproject.org/) instead.

### Trademark notice

"Tor" and the "Onion Logo" are registered trademarks of The Tor Project,
Inc. Content within this project is licensed under a [Creative Commons Attribution
3.0 United States License](https://creativecommons.org/licenses/by/3.0/us/), unless otherwise noted.

Please see our [trademark and copyright policy](https://www.torproject.org/about/trademark/) for more information.