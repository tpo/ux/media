Миссия Tor

Проект Tor создает и распространяет свободные инструменты, позволяющие журналистам, правозащитникам, дипломатам, бизнесменам и другим использовать Интернет без наблюдения со стороны правительства или компаний.

Проект Tor также является глобальным ресурсом для технологий, общественных кампаний, исследований и образования в борьбе за свободу слова, права на приватность онлайн и преодоление цензуры. Проект Tor является ведущей некоммерческой организацией.

Узнайте больше на
https://www.torproject.org/

Присоединяйтесь

Нам нужна Ваша помощь для продолжения этой глобальной работы в ведущейся борьбе за свободу слова, права на приватность онлайн и преодоление цензуры. Присоединяйтесь к нам как спонсор, волонтер, или же участвуйте в будущих мероприятиях, проводимых командой Tor.

Что Tor делает лучше всего

Предоставляет приватность онлайн

Побеждает цензуру

Защищает журналистов

Защищает правозащитников

Защищает жертв домашнего насилия

Держит глобальные каналы информации доступными для всех

Работает с политическими деятелями

Сотрудничает с академическими и исследовательскими институтами


Правоприменение и Проект Tor

Кто использует Tor?

Большая часть пользователей Tor - обычные граждане, которые хотят контролировать свою приватность онлайн, или же пользователи, которым нужно обойти Интернет-цензуру. Преступники же имеют более эффективные пути чем Tor.

Нет журналирования, нет закладок

Пользователи Tor могут полагаться на приватность Tor. По замыслу, оператор узла Tor или кто-то с физическим доступом к этому узлу не может определить IP-адрес пользователя Tor. Постоянный совместный контроль исходного кода Tor академическими сообществами и движениями Open Source гарантирует отсутствие закладок в Tor.

Анонимные горячие линии

Tor предоставляет самую безопасную инфраструктуру для действительно анонимных сообщений, держа информационные каналы безопасными для свидетелей и информаторов.

Операции под прикрытием

Tor используется правоохранительными органами и следователями для анонимного наблюдения за веб-страницами и сервисами подозреваемых. Скрывая личность следователя и его местоположение, Tor может быть ценным инструментом для успешных операций под прикрытием онлайн.

Как работает Tor

Алиса шифрует свой запрос веб-страницы к Бобу три раза и посылает его к первому узлу.

Первый узел снимает первый слой шифрования, но не узнает, что запрос направляется к Бобу.

Второй узел снимает второй слой шифрования и передает запрос веб-страницы далее.

Третий узел снимает последний слой шифрования и передает запрос веб-страницы к Бобу, но он не знает, что этот запрос пришел от Алисы.

Боб не знает, что запрос пришел от Алисы, пока она сама этого не сообщит.

Узнайте больше

Постоянный вклад Tor в образование включает правоприменение и политических деятелей.

Документация Tor и каналы поддержки открыты всем.

Научитесь использовать сервис ExoneraTor для определения, был ли IP-адрес использован узлом Tor.

Узнайте больше, связавшись с командой экспертов Tor.


Польза анонимности онлайн

Действительность

Интернет-провайдеры (такие как BT и Verizon), вебсайты (такие как Google и Facebook) и правительства используют стандартную форму Интернет-слежки, известную как слежение за IP-адресом, для мониторинга общения в публичных сетях.

Новостные сайты могут предлагать разные статьи, основываясь на Вашем местоположении.

Интернет-магазины могут применять ценовую дискриминацию, основываясь на Ваших стране или классе организации.

Типичный человек отслеживается сотнями компаний, продающими профили рекламным агенствам.

Ваша активность в социальных сетях может быть обнаружена и использована против Вас злоумышленниками.

Свобода

Состояние Интернета постоянно изменяется, и тенденции в законе, политике и технологии угрожают анонимности как никогда, подрывая нашу способность говорить и читать свободно онлайн. Страны следят друг за другом и за своими гражданами, блокируют сайты, следят за содержимым интернет-трафика и скрывают важные новости мира.

(Как работает Tor: как сверху)

№1 в приватности онлайн

Tor - свободная технология с открытым исходным кодом, отточенная за более чем 10 лет исследования и разработки командой специалистов по безопасности и разработчиков программного обеспечения.

Tor является одной из самых эффективных технологий для защиты приватности онлайн и гарантии Вашего контроля над персональной безопасностью в сети.


Свобода и приватность онлайн

цензура

(Merriam-Webster, 2012)

Act of changing or suppressing speech or writing that is considered subversive of the common good.

Интернет-цензура

Во всем мире цензура получает новое значение. Запрет доступа к информации и наблюдение за исходящим контентом куда более распространены, чем представляет себе большинство людей. Исследователи цензуры в команде Tor работают над созданием инструментов, опережающих тактики цензуры и предоставляющих открытые каналы коммуникаций для всех онлайн. Команда Tor строит партнерства для повышения осведомленности и обучения людей о необходимости приватности и свободы слова онлайн.

Интернет-слежка повсеместна и проста.

Части сети могут быть под наблюдением.

Соединение Боба может быть под наблюдением или он может быть работающим на режим.

Алиса может быть отслеживаема во время подключения к Бобу.

(Как работает Tor: как сверху)

Некоторые варианты преодоления цензуры используют только один слой для маскировки соединений.

Прокси-сервер может перехватывать трафик или может быть под наблюдением.

К сожалению, один слой (как с прокси) легко атаковать.

