﻿Tors uppdrag

Tor-projektet skapar och distribuerar fria verktyg som låter journalister, människorättsaktivister, diplomater, affärsmän och alla andra använda Internet utan att övervakas av myndigheter eller företag.

Tor-projektet är en global resurs i strävan efter yttrandefrihet, rätten till privatliv och kringgående av censur. Tor-projektet används för forskning & utbildning, teknikutveckling och social påverkan. Tor-projektet är stolta över att vara en av de största ideella organisationerna på internet.

Läs mer på
https://www.torproject.org/

Bli en av oss!

Vi behöver din hjälp för att fortsätta kämpa för yttrandefrihet, kringgående av censur och rätten till privatliv. Du kan engagera dig och vara delaktig i det här globala arbetet! Du kan vara sponsor, frivillig, finansiär, eller bara delta i kommande evenemang ledda av Tor-teamet. Det är upp till dig!

Vad Tor gör bäst

Skyddar din integritet på nätet

Kringgår censur

Skyddar journalister

Skyddar människorättsaktivister

Skyddar offer för våld i hemmet

Håller globala informationskanaler öppna för alla

Jobbar med beslutsfattare

Är partner till akademiska insititutioner för forskning och utveckling


Brottsbekämpning och Tor-projektet

Vem använder Tor?

De allra flesta Tor-användare är vanliga medborgare som vill ha kontroll över sitt privatliv på nätet — eller censurerade användare som behöver tillgång till det fria nätet. För den som vill bryta mot lagen finns mer effektiva alternativ till Tor.

Inga loggar, inga bakdörrar

Tor är uppbyggt omkring vidarebefordrande ”reläer”. En operatör av ett Tor-relä eller någon med fysisk åtkomst till ett Tor-relä kan inte avslöja individuella IP-adresser eller föra användbara loggar över den trafik som flyter genom reläet. Tors källkod revideras kontinuerligt av forskare och experter på öppen källkod. Detta innebär att användare kan förlita sig på Tor inte har några bakdörrar.

Anonyma tips

Tor har den säkraste infrastrukturen för verkligt anonyma tips—en kritisk faktor för att hålla kommunikationskanaler för vittnen och informatörer säkra.

Hemliga operationer

Tor används av brottsbekämpande myndigheter och av utredare för att anonymt övervaka misstänkta personers webbsidor och tjänster. Genom att dölja utredarnas identiteter och platser kan Tor vara ett värdefullt verktyg för framgångsrika hemliga operationer.

Hur Tor fungerar

Alice krypterar sin webbsidesförfrågan tre gånger och skickar den till det första reläet.

Det första reläet tar bort det första krypteringslagret men kan inte se att Alices förfrågan går till Bob.

Det andra reläet tar bort ytterligare ett krypteringslager och skickar webbsidesförfrågan vidare.

Det tredje reläet tar bort det sista krypteringslagret och skickar webbsidesförfrågan till Bob, men Bob kan inte se att förfrågan kommer från Alice.

Bob vet inte att webbsidesförfrågan kom från Alice, såvida hon inte berättar det för honom.

Lär dig mer

Tors engagemang för utbildning inkluderar brottsbekämpande myndigheter och beslutsfattare.

Tors dokumentation- och supportkanaler är öppna för alla.

Lär dig använda tjänsten ExoneraTor för att se om en IP-adress använts av ett Tor-relä.

Lär dig mer genom att kontakta Tors expertteam.


Fördelarna med anonymitet på nätet

Verkligheten

Internetleverantörer (som Telia och Bredbandsbolaget), sajter (som Google och Facebook) och myndigheter använder en vanlig typ av nätövervakning känd som ”IP address tracking” för att övervaka konversationer över publika nätverk.

Nyhetssajter kan visa olika artiklar beroende på var du är geografiskt.

E-handelsplatser kan använda prisdiskriminering baserat på vilket land eller institution du surfar från.

En genomsnittsperson är spårad av över hundra företag som säljer profiler till annonsörer.

Din aktivitet på sociala medier kan avslöjas och användas mot dig av illvilliga individer.

Frihet

Internet är under ständig förändring, och trender inom lag, policy och teknik hotar anonymiteten mer än någonsin. Detta underbygger vår möjlighet att yttra oss och läsa fritt på nätet. Länder bevakar såväl varandra som sina egna medborgare. De blockerar och övervakar trafik, begränsar tillgången till information och utestänger världsnyheter.

(Hur Tor funkar: samma som ovan)

Förstahandsvalet för privatlivet

Tor är fri teknik som bygger på öppen källkod. Mjukvaran har finslipats genom tio års forskning och utveckling av säkerhetsforskare och mjukvaruutvecklare som ingår i Tors team.

Tor är en av de erkänt mest effektiva teknikerna för att skydda ditt privatliv på nätet och se till att det är du som har kontroll över din personliga integritet och säkerhet.


Frihet & privatliv på nätet

censur

(Merriam-Webster, 2012)

Att ändra eller undertrycka tal eller skrift som anses vara subversivt för allmänhetens bästa.

Nätcensur

På ett globalt spelfält får censur en helt ny innebörd. Att begränsa tillgång till information och övervaka utgående innehåll är vanligare än de flesta tror. Censurforskarna på Tor jobbar med att bygga verktyg för att vara steget före censurtaktiker och tillhandahålla öppna kommunikationskanaler för alla på nätet. Tor-teamet bygger partnerskap för att öka medvetenheten om och utbilda människor i vikten av privatliv och yttrandefrihet på nätet.

Nätövervakning är vanligt och enkelt.

Delar av nätverket kan vara övervakat.

Bobs övervakning kan vara övervakad; eller är han kanske en övervakare?

Alice kan övervakas när hon ansluter till Bob.

(Hur Tor funkar: samma som ovan)

Vissa metoder för kringgående av censur använder bara ett lager för att dölja anslutningar.

Proxyn kan avlyssna eller vara övervakad.

När det bara är ett lager (som med en proxy) är det enkelt att attackera.
