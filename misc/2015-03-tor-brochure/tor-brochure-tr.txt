Tor’un Amacı

Tor Projesi, gazeteciler, insan hakları eylemcileri, diplomatlar, iş insanları ve diğer herkesin İnterneti iktidarlar ve ticari kuruluşlar tarafından izlenmeden kullanabilmelerini sağlayan ücretsiz araçlar sağlar ve dağıtır.

Tor Projesi, aynı zamanda, ifade özgürlüğü, online mahremiyet hakkı ve sansürsüz yaşamı hedefleyen teknolojiler, fikir savunuculuğu, araştırma ve eğitim konularında küresel bir kaynaktır. Tor Projesi, ABD yasalarına göre (Madde.501(c)(3)) kar amacı gütmeyen bir örgüttür.

Ayrıntılar için
https://www.torproject.org/

Bize Katılın

İfade özgürlüğü, online mahremiyet hakkı ve sansürden kurtulma konularında yaptığımız küresel çalışmalar için yardımınıza ihtiyacımız var. Sponsor, gönüllü, mali destekçi olun, ya da Tor ekibi tarafından düzenlenen etkinliklere katılın.

Tor En İyi Ne Yapar

Online mahremiyetinizi korur

Sansürden kurtulmayı sağlar

Gazetecileri korur

İnsan hakları savunucularını korur

Aile şiddeti mağdurlarını korur

Online bilgi kanallarının herkese açık olmasını sağlar

Karar vericilerle işbirliği yapar

Akademik ve araştırmacı kurumlarla işbirliği yapar


Güvenlik güçleri ve Tor Projesi

Tor'u Kimler Kullanır?

Tor kullanıcılarının büyük bölümü, online mahremiyetlerini korumak isteyen, ya da İnternet engellerini aşmak isteyen sansür mağduru kişilerdir. Yasaları çiğnemek isteyen suçluların, her zaman Tor kullanmaktan daha etkili seçenekleri olmuştur.

Kayıt Yok, Arka Kapı Yok

Kullanıcılar, mahremiyetleri konusunda Tor'a güvenebilirler. Tor'un tasarımı sayesinde, ne onu işletenler, ne de işletenlerin sistemlerine fiziksel olarak erişebilen kişiler, bir Tor kullanıcısının IP adresini bulamazlar. Akademik çevreler ve açık kaynak toplulukları tarafından sürekli yürütülen inceleme ve denetimler, Tor'da bir arka kapı olmamasını garanti altına alır.

Anonim İhbar Hatları

Tor gerçek anlamda anonim bir ortam sağlamak isteyen ihbar hatları için gerekli en güvenli alt yapıyı sağlar. Görgü tanıklarının ve ihbarcıların iletişim kanallarını güvenli tutmak için kritik bir öneme sahiptir.

Gizli Operasyonlar

Tor, güvenlik güçleri tarafından da, şüphelilerin web sitelerini ve hizmetlerini, kimliklerini belli etmeden izlemek için kullanılır. Soruşturmayı yürütenlerin kimlik ve yer bilgilerini gizleyerek, gizli operasyonların online ortamda başarılı olmasını sağlar.

Tor Nasıl Çalışır

Ayşe, Ahmet'in web sayfasına ulaşmak için, görüntüleme isteğini üç kez kriptolar ve ilk Tor aktarıcısına gönderir.

İlk aktarıcı, birinci kripto katmanını kaldırır ama sayfa isteğinin Ahmet'e gittiğini bilmez.

İkinci aktarıcı, diğer kripto katmanını da kaldırır ve web sayfası isteğini sonrakine aktarır.

Üçüncü aktarıcı, son kripto katmanını kaldırır ve web sayfası isteğini Ahmet'e aktarır, ama bu isteğin Ayşe'den geldiğini bilmez.

Ahmet ise, Ayşe ona söylemedikçe web sayfası görüntüleme isteğinin Ayşe'den geldiğini bilmez.

Ayrıntılı Bilgi

Tor kendini eğitime adamıştır. Verdiği eğitimler, güvenlik güçleri ve karar vericilere de yöneliktir.

Tor ile ilgili bilgiler ve destek kanalları herkese açıktır.

Bir IP adresinin Tor aktarıcısı tarafından kullanılmış olup olmadığını öğrenmek için ExoneraTor servisini kullanın.

Tor'un uzman ekibine ulaşarak daha fazla bilgi edinin.


İnternette Anonim Kalmanın Yararları

Gerçekler

İnternet Servis Sağlayıcıları (TTNet gibi), web siteleri (Google, Facebook gibi) ve iktidarlar, çoğunlukla IP adreslerinin izlenmesi yoluyla, İnternet ağlarındaki iletişimi gözlem altında tutarlar.

Haber siteleri bulunduğunuz yere göre farklı haberleri öne çıkarırlar.

Alışveriş siteleri bulunduğunuz ülkeye ya da ait olduğunuz kuruma göre farklı fiyatlar gösterebilirler.

Ortalama bir kullanıcı, reklamcılara kişisel bilgilerini satan yüzden fazla şirket tarafından izlenir.

Sosyal medyadaki etkinlikleriniz, kötü niyetli kişiler tarafından ortaya çıkartılıp size karşı kullanılabilir.

Özgürlük

İnternet dünyası durmaksızın değişmektedir. Yasal düzenlemelerdeki, politikalardaki ve teknolojideki gelişmeler, anonim kalmayı bugüne kadar hiç olmadığı kadar zorlaştırmakta, İnternette özgürce okumayı ve konuşmayı olanaksız hale getirmektedir. Devletler kendi vatandaşlarını izledikleri gibi, birbirlerini de izlemekte, web sitelerini engellemekte, İnternet trafiğinin içeriğini gözlemekte, önemli dünya haberlerini kısıtlamaktadırlar.

(How Tor Works: same as above)

Online Mahremiyet Konusunda 1 Numara

Tor ücretsiz, açık kaynaklı bir teknolojidir; güvenlik uzmanlarından ve yazılım geliştiricilerinden oluşan bir ekip tarafından 10 yıldan fazladır sürekli iyileştirilmektedir.

Tor İnternette mahremiyetinizi korumanızı, kişisel güvenliğinizin kendi kontrolünüzde kalmasını sağlayan en etkili teknolojilerden biridir.


Online Özgürlük ve Mahremiyet

Sansür

(Merriam-Webster, 2012'den çeviri)

Kamu yararına zarar verdiği düşünülen konuşma ya da yazıların denetlenerek değiştirilmesi ve baskı altına alınması.

Online Sansür

Küresel bir mecrada, sansür tamamen yeni bir anlam kazanmaktadır. Çoğumuz, bilgiye erişimin kısıtlanmasının ne kadar yaygın olduğunu, yayınlanan içeriklerin ne kadar yakından gözlem altında tutulduğunu bilmeyiz. Tor projesinde çalışan sansür araştırmacıları, herkesin online iletişim olanaklarının açık kalmasını ve sansürleme taktiklerinin bir adım önünde gidebilmeyi sağlayan araçlar geliştirmektedirler. Tor ekibi, online mahremiyetin ve ifade özgürlüğünün önemi konusunda toplumu bilinçlendirecek, farkındalık yaratacak işbirlikleri kurmaktadır.

İnternetin takibi çok yaygın, aynı zamanda da kolaydır.

İnternet ağının bazı bölümleri takip altına alınabilir.

Ahmet'in İnternet bağlantısı izlenebilir ya da Ahmet iktidara hizmet eden biri olabilir.

Ayşe, Ahmet ile bağlantı kurduğu sırada izlenebilir.

(How Tor Works: same as above)

Bazı sansürden kurtulma sistemleri bağlantıları gizlemek için yalnızca tek bir katman kullanmaktadır.

Proxy'ler sizi izleyebilir ya da izlenebilirler.

Proxy'ler gibi tek katmanlı sistemler, ne yazık ki çok kolay kırılabilir.

